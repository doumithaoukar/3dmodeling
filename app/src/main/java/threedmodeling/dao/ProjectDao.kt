package threedmodeling.dao

import androidx.room.Dao
import androidx.room.Query
import threedmodeling.data.Project
import threedmodeling.database.BaseDao

@Dao
abstract class ProjectDao : BaseDao<Project> {

    @Query("SELECT * FROM project")
    abstract fun getAll(): List<Project>

    @Query("SELECT * FROM project where isRemoved =0")
    abstract fun getActiveProjects(): List<Project>

    @Query("SELECT * FROM project")
    abstract fun getHistory(): List<Project>

    @Query("DELETE FROM Project WHERE id=:projectId")
    abstract fun delete(projectId: Int)

    @Query("Update Project set isRemoved=1  WHERE id=:projectId")
    abstract fun remove(projectId: Int)


}



