package threedmodeling.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.threedmodeling.databinding.ItemProjectBinding
import threedmodeling.data.Project


class ProjectAdapter(var items: List<Project>) :
    RecyclerView.Adapter<ProjectAdapter.ProjectViewHolder>() {

    var onItemClickListener: OnItemClickListener? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: ItemProjectBinding =
            ItemProjectBinding.inflate(layoutInflater, parent, false)
        return ProjectViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) {
        holder.bind(items[position], onItemClickListener)
    }


    class ProjectViewHolder(var binding: ItemProjectBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(project: Project, onItemClickListener: OnItemClickListener?) {
            binding.name.text = project.name
            binding.root.setOnClickListener {
                onItemClickListener?.onItemClicked(project)
            }
            binding.root.setOnLongClickListener {
                onItemClickListener?.onItemLongClicked(project)
                true
            }
        }
    }


    interface OnItemClickListener {
        fun onItemLongClicked(project: Project)
        fun onItemClicked(project: Project)
    }

}