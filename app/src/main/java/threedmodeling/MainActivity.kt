package threedmodeling

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.fragment.NavHostFragment
import com.threedmodeling.R
import com.threedmodeling.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity(), MainActivityListener {


    private lateinit var navController: NavController
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.mainFrame) as NavHostFragment
        navController = navHostFragment.navController

        binding.bottomNavigationView.selectedItemId = R.id.navFilesFragment
        binding.bottomNavigationView.setOnNavigationItemSelectedListener { menuItem ->
            menuItem.isChecked = true
            navController.popBackStack()
            navController.navigate(menuItem.itemId)
            true

        }

        navController.addOnDestinationChangedListener { _: NavController?, destination: NavDestination, _: Bundle? ->
            when(destination.id){
                R.id.navAddFragment->{
                    binding.toolbar.title = getString(R.string.add)

                }
                R.id.navFilesFragment->{
                    binding.toolbar.title = getString(R.string.files)

                }
                R.id.navHistoryFragment->{
                    binding.toolbar.title = getString(R.string.history)
                }
            }
        }

    }

    override fun openFiles() {
        navController.navigate(R.id.navFilesFragment)
    }
}