package threedmodeling.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import threedmodeling.dao.ProjectDao
import threedmodeling.data.Project

@Database(entities = [Project::class], version = 1)


abstract class AppDatabase : RoomDatabase() {

    abstract fun projectDao(): ProjectDao


    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext, AppDatabase::class.java,
                    "3dModelling"
                ).allowMainThreadQueries().build()
            }
            return INSTANCE as AppDatabase
        }
    }
}