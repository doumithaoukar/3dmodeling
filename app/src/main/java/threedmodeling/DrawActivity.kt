package threedmodeling

import android.R
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.threedmodeling.databinding.ActivityDrawBinding
import threedmodeling.data.Project
import threedmodeling.opengl2.Cube
import threedmodeling.opengl2.MyGLSurfaceView


class DrawActivity : AppCompatActivity() {

    companion object {
        val PROJECT_EXTRA = "project_extra"

    }

    var project: Project? = null
    private lateinit var binding: ActivityDrawBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDrawBinding.inflate(layoutInflater)
        setContentView(binding.root)
        project = intent.getParcelableExtra(PROJECT_EXTRA)

        supportActionBar?.title = project?.name
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        // project dimensions
        project?.let {
            Cube.setDimensions(it.width, it.height, it.depth)
        };

        // demo scale
        Cube.setScale(0.25f);

        val view = MyGLSurfaceView(this)
        binding.surfaceView.addView(view)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        return if (id == R.id.home) {
            //your code
            this.onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }
}