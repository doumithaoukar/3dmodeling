package threedmodeling.views

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.threedmodeling.R
import com.threedmodeling.databinding.FragmentFilesBinding
import com.threedmodeling.databinding.FragmentHistoryBinding
import threedmodeling.DrawActivity
import threedmodeling.adapters.ProjectAdapter
import threedmodeling.data.Project
import threedmodeling.database.AppDatabase

class HistoryFragment : Fragment() {

    private var _binding: FragmentHistoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setAdapter()

    }

    private fun setAdapter() {
        context?.let { mContext ->
            val appDatabase = AppDatabase.getAppDatabase(mContext)
            val list = appDatabase.projectDao().getAll()
            val adapter = ProjectAdapter(list)
            val layoutManager = LinearLayoutManager(mContext)
            _binding?.recyclerView?.layoutManager = layoutManager
            _binding?.recyclerView?.adapter = adapter
            adapter.onItemClickListener = object : ProjectAdapter.OnItemClickListener {
                override fun onItemLongClicked(project: Project) {
                    AlertDialog.Builder(mContext)
                        .setMessage(getString(R.string.confirm_delete))
                        .setPositiveButton(R.string.Yes) { dialog, _ ->
                            appDatabase.projectDao().delete(project)
                            setAdapter()
                            dialog.dismiss()
                        }
                        .setNegativeButton(android.R.string.no, null)
                        .show()
                }

                override fun onItemClicked(project: Project) {
                    val intent = Intent(mContext, DrawActivity::class.java)
                    intent.putExtra(DrawActivity.PROJECT_EXTRA, project)
                    startActivity(intent)
                }

            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}