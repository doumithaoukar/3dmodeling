package threedmodeling.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.threedmodeling.R
import com.threedmodeling.databinding.FragmentAddBinding
import threedmodeling.views.dialogs.CreateShapeDialogFragment

class AddFragment : Fragment(), View.OnClickListener {

    private var _binding: FragmentAddBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentAddBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.addCube?.setOnClickListener(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.addCube -> {
                val dialog = CreateShapeDialogFragment()
                dialog.show(
                    childFragmentManager.beginTransaction(),
                    CreateShapeDialogFragment::class.simpleName
                )
            }
        }
    }
}