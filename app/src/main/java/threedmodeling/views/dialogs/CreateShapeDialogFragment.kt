package threedmodeling.views.dialogs

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.threedmodeling.R
import com.threedmodeling.databinding.FragmentAddBinding
import com.threedmodeling.databinding.FragmentCreateShapeBinding
import threedmodeling.DrawActivity
import threedmodeling.MainActivityListener
import threedmodeling.data.Project
import threedmodeling.database.AppDatabase

class CreateShapeDialogFragment : DialogFragment(), View.OnClickListener {

    private var _binding: FragmentCreateShapeBinding? = null
    private val binding get() = _binding!!
    private var appDatabase: AppDatabase? = null

    private var listener: MainActivityListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateShapeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = try {
            activity as MainActivityListener
        } catch (e: ClassCastException) {
            throw ClassCastException(activity.toString() + " must implement MainActivityListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding?.btnCancel?.setOnClickListener(this)
        _binding?.btnOk?.setOnClickListener(this)
        context?.let { mContext ->
            appDatabase = AppDatabase.getAppDatabase(mContext)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        listener = null
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnCancel -> {
                dismiss()
            }
            R.id.btnOk -> {
                val name = _binding?.nameValue?.text?.toString() ?: ""
                val width = _binding?.widthValue?.text?.toString()?.toFloatOrNull() ?: 0f
                val height = _binding?.heightValue?.text?.toString()?.toFloatOrNull() ?: 0f
                val depth = _binding?.depthValue?.text?.toString()?.toFloatOrNull() ?: 0f
                val project = Project(0, name, width, height, depth)
                appDatabase?.projectDao()?.insert(project)
                val intent = Intent(context, DrawActivity::class.java)
                intent.putExtra(DrawActivity.PROJECT_EXTRA, project)
                startActivity(intent)
                dismiss()
                listener?.openFiles()
            }
        }
    }
}